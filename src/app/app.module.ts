import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http'
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { AuthService } from './auth/auth-service'
import { UserService } from './auth/user-service'
import { LoginComponent } from './login/login.component'
import { RegisterComponent } from './register/register.component'
import { AuthGuard} from './forbidden/auth-guard'
import { AlertService} from './auth/alert.service'
import { DashboardComponent } from './dashboard/dashboard.component'
import {MatButtonModule, MatCardModule } from '@angular/material';
import { AlertComponent } from './directives/alert.compoenent';



@NgModule({
  imports: [
    BrowserModule,FormsModule, HttpModule, routing, MatButtonModule, MatCardModule
  ],
  declarations: [
    AppComponent, AlertComponent, DashboardComponent, LoginComponent, RegisterComponent
  ],
  
  providers: [AuthService, AlertService, AuthGuard, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
