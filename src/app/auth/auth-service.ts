import { Injectable } from '@angular/core';
import { Http, Headers,RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthService {
    public token: string;

    constructor(private http: Http) {
    }

    login(email: string, password: string): Observable<boolean> {

      
        return this.http.post('http://127.0.0.1:8000/api_auth/login/', JSON.stringify({ email: email, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                
                    // store email and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                    // return true to indicate successful login
                    return user;
          
            });
    }

    logout(): void {
        localStorage.removeItem('currentUser');
    }
}